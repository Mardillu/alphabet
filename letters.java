import java.util.Scanner;
public class letters{  
  public static void a(){
    System.out.println("            a");  
    System.out.println("          a a");
    System.out.println("         a   a");
    System.out.println("        a     a");
    System.out.println("       a       a");
    System.out.println("    a a a a a a a a");
    System.out.println("     a           a");
    System.out.println("    a             a");
    System.out.println("   a               a");
    System.out.println("  a                 a");
    System.out.println(" a                   a");
    System.out.println("a                     a");
  }
  
  public static void b(){
    System.out.println(" b b b b b");  
    System.out.println(" b         b");
    System.out.println(" b          b");
    System.out.println(" b           b");
    System.out.println(" b          b");
    System.out.println(" b         b");
    System.out.println(" b b b b");
    System.out.println(" b         b");
    System.out.println(" b           b");
    System.out.println(" b            b");
    System.out.println(" b           b");
    System.out.println(" b b b b b");
  }
  
  public static void c(){
    System.out.println("           c c ");  
    System.out.println("       c c  ");  
    System.out.println("    c c ");
    System.out.println("  c ");
    System.out.println(" c ");
    System.out.println("c ");
    System.out.println("c ");
    System.out.println("c");
    System.out.println( "c ");
    System.out.println("  c ");
    System.out.println("    c ");
    System.out.println("       c c  ");
    System.out.println("           c c ");
  }
  public static void d(){
    System.out.println("d d d d d ");  
    System.out.println("d        d");  
    System.out.println("d          d");
    System.out.println("d            d");
    System.out.println("d             d");
    System.out.println("d              d");
    System.out.println("d               d");
    System.out.println("d               d");
    System.out.println("d               d");
    System.out.println("d              d");
    System.out.println("d             d");
    System.out.println("d            d");
    System.out.println("d          d");
    System.out.println("d        d");
    System.out.println("d d d d");
  }
  public static void e(){
    System.out.println("e e e e e e e e ");  
    System.out.println("e");  
    System.out.println("e");
    System.out.println("e");
    System.out.println("e");
    System.out.println("e");
    System.out.println("e");
    System.out.println("e e e e e e ");
    System.out.println("e");
    System.out.println("e");
    System.out.println("e");
    System.out.println("e");
    System.out.println("e");
    System.out.println("e");
    System.out.println("e e e e e e e e ");
  }
  public static void f(){
    System.out.println("f f f f f f f    ");  
    System.out.println("f");  
    System.out.println("f");
    System.out.println("f");
    System.out.println("f");
    System.out.println("f");
    System.out.println("f");
    System.out.println("f f f f f ");
    System.out.println("f");
    System.out.println("f");
    System.out.println("f");
    System.out.println("f");
    System.out.println("f");
    System.out.println("f");
    System.out.println("f ");
  }  
  private static void printG(){
    System.out.println("     GGGGGGG");
    System.out.println("   G       GGG");
    System.out.println("  GG");
    System.out.println(" GG");
    System.out.println("GG");
    System.out.println("GG     GGGGGG");
    System.out.println("GG         GG");
    System.out.println(" GG        GG");
    System.out.println("   GG     GG");
    System.out.println("    GGGGGG");
   }
   private static void printH(){
    System.out.println("HH        HH");
    System.out.println("HH        HH");
    System.out.println("HH        HH");
    System.out.println("HH        HH");
    System.out.println("HHHHHHHHHHHH");
    System.out.println("HHHHHHHHHHHH");
    System.out.println("HH        HH");
    System.out.println("HH        HH");
    System.out.println("HH        HH");
    System.out.println("HH        HH");
  }
   private static void printI(){
    System.out.println("II");
    System.out.println("II");
    System.out.println("II");
    System.out.println("II");
    System.out.println("II");
    System.out.println("II");
    System.out.println("II");
    System.out.println("II");
    System.out.println("II");
    System.out.println("II");
   }
   private static void printJ(){
    System.out.println("JJJJJJJJJJJJ");
    System.out.println("          JJ");
    System.out.println("          JJ");
    System.out.println("          JJ");
    System.out.println("          JJ");
    System.out.println("          JJ");
    System.out.println("          JJ");
    System.out.println("J         JJ");
    System.out.println("JJ     JJJ");
    System.out.println(" JJJJJJJ");
   }

   private static void printK(){
    System.out.println("KK    KK");
    System.out.println("KK   KK");
    System.out.println("KK  KK");
    System.out.println("KK KK");
    System.out.println("KKKK");
    System.out.println("KKKK");
    System.out.println("KK KK");
    System.out.println("KK  KK");
    System.out.println("KK   KK");
    System.out.println("KK    KK");
  }
  private static void printQ(){
    System.out.println("  QQQQQQ  ");
    System.out.println(" QQ     QQ");
    System.out.println("QQ       QQ");
    System.out.println("QQ       QQ");
    System.out.println("QQ       QQ");
    System.out.println("QQ       QQ");
    System.out.println("QQ  QQ   QQ");
    System.out.println(" QQ  QQ QQ");
    System.out.println("  QQQQQQQ");
    System.out.println("       QQ");
  }
  
  private static void printR(){
    System.out.println("RRRRRR");
    System.out.println("RR    RR");
    System.out.println("RR     RR");
    System.out.println("RR     RR");
    System.out.println("RR   RR");
    System.out.println("RR RR");
    System.out.println("RR    RR");
    System.out.println("RR     RR");
    System.out.println("RR     RR");
    System.out.println("RR     RR");
  }
  
  private static void printS(){
    System.out.println("   SSSS");
    System.out.println(" SS    SS");
    System.out.println("SS      SS");
    System.out.println("SS");
    System.out.println("  SSS");
    System.out.println("     SSS");
    System.out.println("       SS");
    System.out.println("SS     SS");
    System.out.println(" SS   SS");
    System.out.println("   SSSS");
  }
  
  private static void printT(){
    System.out.println("TTTTTTTTTTT");
    System.out.println("TTTTTTTTTTT");
    System.out.println("    TT");
    System.out.println("    TT");
    System.out.println("    TT");
    System.out.println("    TT");
    System.out.println("    TT");
    System.out.println("    TT");
    System.out.println("    TT");
    System.out.println("    TT");
  }
  
  private static void printU(){
    System.out.println("UU       UU");
    System.out.println("UU       UU");    
    System.out.println("UU       UU");
    System.out.println("UU       UU");
    System.out.println("UU       UU");
    System.out.println("UU       UU");
    System.out.println("UU       UU");
    System.out.println("UUU    UUUU");
    System.out.println(" UUUU UUUU");
    System.out.println("   UUUUU");
  }
  public static void letterV(){
    System.out.println("vv                    vv");
    System.out.println("  vv                vv"); 
    System.out.println("    vv           vv");
    System.out.println("     vv        vv");
    System.out.println("       vv    vv");
    System.out.println("         vvvv");
  }
  
  public static void letterW(){
    System.out.println("ww              wwww              ww");
    System.out.println(" ww            wwwwww           ww");
    System.out.println("  ww         ww     ww        ww");
    System.out.println("    ww     ww         ww    ww");
    System.out.println("      ww ww             wwww");
    System.out.println("        ww               ww");
  }
  public static void letterX(){
    System.out.println("xx            xx");
    System.out.println("  xx        xx");
    System.out.println("    xx     xx");
    System.out.println("       xx   ");
    System.out.println("    xx    xx");
    System.out.println("  xx       xx");
    System.out.println("xx           xx");
  }
  public static void letterY(){
    System.out.println("yy          yy");
    System.out.println("  yy      yy");
    System.out.println("   yy  yy");
    System.out.println("     yy"); 
     System.out.println("    yy");
      System.out.println("   yy");
  }
  public static void letterZ(){
    System.out.println("  zzzzzzzzzz");
    System.out.println("         zz");
    System.out.println("       zz");
    System.out.println("      zz");
    System.out.println("     zz" );
    System.out.println("    zz");
    System.out.println("    zzzzzzzzzz");
    
  }
  
  public static void main (String[] args){
    Scanner keyboard;
    String terminate = "y";
    String cmd="";
    System.out.println("Hi. I can print out alphabets by using their small letters. Please input which alphabet you want to type");
    while (terminate.equalsIgnoreCase("y")){
      keyboard = new Scanner(System.in);
      cmd = keyboard.next();
      switch (cmd){
        case "a": a();
        break;
        case "b": b();
        break;
        case "c": c();
        break;
        case "d": d();
        break;
        case "e": a();
        break;
        case "f": f();
        break;
        case "g": printG();
        break;
        case "h": printH();
        break;
        case "i": printI();;
        break;
        case "j": printJ();
        break;
        case "k": printK();
        break;
        /*case "l": printL();
         break;
         case "m": printM();
         break;
         case "n": printN();
         break;
         case "o": printO();
         break;
         case "p": printP();
         break;
         */
        case "q": printQ();
        break;
        case "r": printR();
        break;
        case "s": printS();
        break;
        case "t": printT();
        break;
        case "u": printU();
        break;case "v": letterV();
        break;
        case "w": letterV();
        break;
        case "x": letterX();
        break;
        case "y": letterY();
        break;
        case "z": letterZ();
        break;
        default: a();
        System.out.println();
        System.out.println();
        b();
        System.out.println();
        System.out.println();
        c();
        System.out.println();
        System.out.println();
        d();
        System.out.println();
        System.out.println();
        e();
        System.out.println();
        System.out.println();
        f();
        break;
      }
      System.out.println("Would you like to type out another alphabet? y/n");
      terminate = keyboard.next();
    }
  }
}